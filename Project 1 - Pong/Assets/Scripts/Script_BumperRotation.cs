﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Michael Nelson

public class Script_BumperRotation : MonoBehaviour {

    public float leftPaddleRotationMax = 25;
    public float rightPaddleRotationMax = -25;
    public GameObject leftPaddle;
    public GameObject rightPaddle;
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.LeftShift) && leftPaddle.transform.rotation.z < leftPaddleRotationMax)
            leftPaddle.transform.Rotate(0, 0, 1);

        else if (leftPaddle.transform.rotation.z > 0)
        {
            leftPaddle.transform.Rotate(0, 0, -1);

            if (leftPaddle.transform.rotation.z < 0)
                leftPaddle.transform.Rotate(0, 0, 1); ;
        }

        if (Input.GetKey(KeyCode.RightShift) && rightPaddle.transform.rotation.z > rightPaddleRotationMax)
            rightPaddle.transform.Rotate(0, 0, -1);

        else if (rightPaddle.transform.rotation.z < 0)
        {
            rightPaddle.transform.Rotate(0, 0, 1);

            if (rightPaddle.transform.rotation.z > 0)
                rightPaddle.transform.Rotate(0, 0, -1); ;
        }

    }
}

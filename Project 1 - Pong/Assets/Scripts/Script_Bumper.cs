﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Jeffery Luther

public class Script_Bumper : MonoBehaviour
{
  public GameObject RightBumper;
  public GameObject LeftBumper;
  public int leftBumperSpeed = 10;
  public int rightBumperSpeed = 10;

	// Update is called once per frame
	void Update ()
  {
		if(Input.GetKey(KeyCode.W) && LeftBumper.transform.position.y < 4 && LeftBumper.transform.rotation == Quaternion.identity)
    {
      LeftBumper.transform.Translate(Vector3.up * leftBumperSpeed * Time.deltaTime);
    }

    if(Input.GetKey(KeyCode.S) && LeftBumper.transform.position.y > -4 && LeftBumper.transform.rotation == Quaternion.identity)
    {
      LeftBumper.transform.Translate(Vector3.down * leftBumperSpeed * Time.deltaTime);
    }

    if (Input.GetKey(KeyCode.UpArrow) && RightBumper.transform.position.y < 4 && RightBumper.transform.rotation == Quaternion.identity)
    {
      RightBumper.transform.Translate(Vector3.up * rightBumperSpeed * Time.deltaTime);
    }

    if (Input.GetKey(KeyCode.DownArrow) && RightBumper.transform.position.y > -4 && RightBumper.transform.rotation == Quaternion.identity)
    {
      RightBumper.transform.Translate(Vector3.down * rightBumperSpeed * Time.deltaTime);
    }
  }
}
